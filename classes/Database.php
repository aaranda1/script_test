<?php

class DataBase {

    public function __construct($host, $username, $password, $dbName)
    {
       try {

        $this->connection = new mysqli($host, $username, $password); //the database connection is created

        if (!$this->connection->connect_error) {

            echo '[INFO] Conected to host: '. $host.PHP_EOL;

            if(!$this->findDB($dbName)){ // the connection looks for a database that matchs the DB_NAME to connect to it

                echo '[INFO] Database '. $dbName. ' does not exist'.PHP_EOL;

                $this->createDB($dbName); //if no database is found, a new databas is created 
                
    
            }
    
            $this->useDB($dbName); //whether the database was found or created, the connection is made
            $this->connection->autocommit(0); //the autocommited is set to 0 so rollbacks can be achived
            echo '[INFO] Conected to database: '. $dbName.PHP_EOL;
           
            return;
        }
        else{
            
            throw new Exception($this->connection->connect_error); // in case of a connection error, a exception is thrown

        }


       } catch (\Exception $th) {
           
            throw $th;
       }

    }


    public function query($query){ // all queries executed through the code, are made by this function

        $result = $this->connection->query($query);

        if (!$this->connection->error) {

            return $result;
            
        } else {

            throw new Exception($this->connection->error);
        }

        return ;

    }


    private function createDB($dbName){

        try {
            echo '[INFO] Creating Database: '. $dbName.PHP_EOL;

            return $this->query('CREATE DATABASE '.$dbName);

        } catch (\Exception $th) {

            throw $th;
        }

    }

    private function findDB($dbNAme){
    
        return in_array($dbNAme, $this->getDBs());

    }
    private function useDB($dbName){

        echo '[INFO] Conecting to database: '.$dbName.PHP_EOL;

        try {

            $this->name = $dbName;

            $this->query('USE '.$dbName.';');

            return;

        } catch (\Exception $th) {

            throw $th;
        }

    }
    public function getTables(){

        $tables = [];

        try {
            $rows = $this->query('SHOW TABLES');
            while($row = $rows->fetch_assoc()){

                array_push($tables , $row['Tables_in_'.$this->name]);
    
            }

            return $tables;

        } catch (\Exception $th) {

            throw $th;
        }
        

    }

    private function tableExist($tableName){
    
        return in_array($tableName, $this->getTables());

    }

    private function getDBs(){

        $databases = [];

        try {
            $rows = $this->query('SHOW DATABASES');
            while($row = $rows->fetch_assoc()){
    
                array_push($databases , $row['Database']);
    
            }
            return $databases;

        } catch (\Exception $th) {

            throw $th;
        }
        

    }
    private function buildMigrationQuery($migration){

        $columnsQuery = '';

        foreach ($migration['columns'] as $key => $column) {

            $columnsQuery = $columnsQuery.$column['name'].' '.$column['type'].' '.implode($column['constrains'],' ');

            $columnsQuery =  ($key == sizeof($migration['columns']) -1) ? $columnsQuery.'' : $columnsQuery.',';

        }
        
        return 'CREATE TABLE '.$migration['name'].'('.$columnsQuery.');';


        
    }
    private function dropAllTables(){

        try {
            $tables = $this->getTables();

            foreach ($tables as $table) {

                $this->dropTable($table);
            }

        } catch (\Exception $th) {

            throw $th;
        }


    }
    private function dropTable($tableName){


        try {

            $this->query('DROP TABLE '.$tableName.';');

        } catch (\Exception $th) {

            throw $th;
        }

    }
    private function buildSeedQuery($tableName, $rows){

        try {

            $columnNames = implode(
                array_reverse(
                    array_keys( $rows[0] )
                )
                ,', '
            );
                    
            $rowQuery = '';
    
            while ($row = array_pop($rows)) {

                $rowQuery = $rowQuery.'(';

                while ($data = array_pop($row)) { 

                    $rowQuery = $rowQuery."'".$data."'";
                    $rowQuery =  empty($row) ? $rowQuery.'' : $rowQuery.',';
                   
                }

                $rowQuery = $rowQuery.')';
                $rowQuery =  empty($rows) ? $rowQuery.'' : $rowQuery.',';
    
            }
            
            return 'INSERT INTO '.$tableName.'('.$columnNames.') VALUES '.$rowQuery.' ;';

        } catch (\Exception $th) {
            throw $th;
        }
        
    }
    public function seed($tableName, $data){

        if($this->tableExist($tableName) ){

            $query = $this->buildSeedQuery($tableName, $data);
            return $this->query($query);

         }

    }

    public function migrate($migrations, $refresh = true){ //migrations are runed, the refresh variable sets if all the database tables are droped so the migrations can be runed from scratch

        try {

            if($refresh){

                $this->dropAllTables();

            }

            foreach ($migrations as $table) {


                if($this->tableExist($table['name']) ){

                   throw new Exception('table '.$table['name'].' already exist');
                }

                $query =  $this->buildMigrationQuery($table);

                $this->query($query);
                
            }

        } catch (\Exception $th) {

            throw $th;
        }

    }

    public function beginTransaction(){

        try {
            
            return $this->query('BEGIN;');
            
        } catch (\Exception $th) {

            throw $th;
        }


        
    }
    public function commit(){
        try {

            return $this->query('COMMIT;');
        } catch (\Exception $th) {

            throw $th;
        }

        
    }
    public function rollback(){
        try {
            return $this->query('ROLLBACK;');
        } catch (\Exception $th) {

            throw $th;
        }

        
    }

}