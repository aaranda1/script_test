<?php

require(dirname(dirname(__FILE__)).'/constants/migrations.php');


$userTable = [
    'name' => USERS_TABLE,
    'columns' => [
        [
            'name' => 'name',
            'type' => VARCHAR,
            'constrains' => []
        ],
        [
            'name' => 'surname',
            'type' => VARCHAR,
            'constrains' => []
        ],
        [
            'name' => 'email',
            'type' => VARCHAR,
            'constrains' => [NOT_NULL, PK]
        ]
    ]
]



?>