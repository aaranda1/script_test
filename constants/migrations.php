<?php

//table name
define('USERS_TABLE', 'users');

//data types
define('VARCHAR', 'varchar(30)');

//constrains
define('NOT_NULL', 'NOT NULL');
define('PK', 'PRIMARY KEY');