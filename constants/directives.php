<?php

//options types
define('SHORT_OPTION', 0);
define('LONG_OPTION', 1);

//individual Directives
define('USER', 'u');
define('HOST', 'h');
define('PASSWORD', 'p');
define('DRY_RUN', 'dry_run');
define('CREATE_TABLE', 'create_table');
define('FILE', 'file');
define('HELP', 'help');



//directives dictionary
define('DIRECTIVES', [
    USER => [
        'required' => true,
        'option_type' => SHORT_OPTION,
        'description' => 'MySQL username'
    ],
    HOST => [
        'required' => true,
        'option_type' => SHORT_OPTION,
        'description' => 'MySQL host'
    ],
    PASSWORD => [
        'required' => true,
        'option_type' => SHORT_OPTION,
        'description' => 'MySQL password'
    ],
    DRY_RUN => [
        'required' => false,
        'option_type' => LONG_OPTION,
        'description' => 'This will be used with the --file directive in case we want to run the script but not insert into the DB. All other functions will be executed, but the database wont be altered'
    ],
    CREATE_TABLE => [
        'required' => false,
        'option_type' => LONG_OPTION,
        'description' => 'This will cause the MySQL users table to be built (and no further action will be taken)'
    ],
    FILE => [
        'required' => true,
        'option_type' => LONG_OPTION,
        'description' => 'This is the name of the CSV to be parsed'
    ],
    HELP => [
        'required' => false,
        'option_type' => LONG_OPTION,
        'description' => 'Will output the list of directives with details'
    ],

]);


function displayDirectives(){

    foreach (DIRECTIVES as $directive => $data) {

        $option = ($data['option_type'] == LONG_OPTION) ? '--' : '-';
        $command = $option.$directive;

        echo "\033[33m $command \033[0m";
        echo '   '.$data['description'].PHP_EOL;
        

        
    }


}

function validateDirectives($directives){

    foreach (DIRECTIVES as $directive => $data) {
        if($data['required'] && ! isset($directives[$directive])){

            $option = ($data['option_type'] == LONG_OPTION) ? '--' : '-';

            throw new Exception('the '.$option.$directive.' directive is required');

        }
        
    }

    return true;


}



?>