<?php

function standarize($name){

    return str_replace(
        "'",
        ' ',
        ucfirst(
            strtolower($name)
        )
    );
}
function validateEmail($email){

    if(filter_var($email, FILTER_VALIDATE_EMAIL)){

        return true;
    }
    else{

        throw new Exception($email. 'is not a valid email');
        
    }
}

function getUser($fileRoute){

    $users = [];
    try {
        if($file = fopen($fileRoute,"r")){

            $columns = fgetcsv($file,1000);
        
            while ($row = fgetcsv($file,1000)) {
        
                foreach ($columns as $key => $column) {
            
                    if(trim($column) == 'email')
                        validateEmail($row[$key]);
                    else
                        $row[$key] = standarize($row[$key]);
            
            
                    $user[trim($column)] = $row[$key];
                    
                }
                
                array_push($users,$user);
            }
    
        }
        else{
    
            throw new Exception("unable to open file ".$fileRoute);
            
        }
    } catch (\Exception $th) {

        throw $th;

    }

    return $users;

}


?>