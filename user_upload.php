<?php

require('classes/Database.php');
require('constants/directives.php');
require('migrations/index.php');
require('seeds/user_seeder.php');

define('DB_NAME', 'myDB');

try {

    $directives = getopt(HOST.':'.USER.':'.PASSWORD.':', [DRY_RUN, CREATE_TABLE, HELP, FILE.':']); //the commands are fetched

    if(!isset($directives[HELP])){

        validateDirectives($directives); // the directives are validated based on how required are. EXAMPLE : -h -p -u are required because without them the database connection cant be achived

        $database = new DataBase($directives[HOST], $directives[USER], $directives[PASSWORD], DB_NAME); //a database class is instantiated to haddle all database operations
        
        $database->beginTransaction(); //a database transaction is initiated, the database class constructor set de autocommit to 0 so rollbacks can be achived

        $database->migrate(MIGRATIONS); // in the ./migrations folder the users table migration is specified to the database class can process it and migrate it, 


        if(!isset($directives[CREATE_TABLE])){ //depending on the --create_table directive, tha users table is seeded

            $database->seed(USERS_TABLE, getUser($directives[FILE]));    
          
        }
        
        return isset($directives[DRY_RUN]) // depending on the --dry_run directive, all operations are commited or discarted
        ?   $database->rollback()
        :   $database->commit();
    }
    else{

        return displayDirectives(); // depending on the --help directive, all commands are showed through the console
    }


} catch (\Exception $th) {

    echo '[ERROR] '.$th->getMessage().PHP_EOL;

    return (isset($database))  // in case of an exception, the database is rolled back
    ?   $database->rollback()
    :   null;

}